/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlleonding.data;
/**
 *
 * @author Rene
 */
public class Message {
    public String topic;
    public String value;
    public String unit;
    public String timestamp;
    
    public Message(String topic, String value, String unit, String timestamp){
        this.topic = topic;
        this.value = value;
        this.unit = unit;
        this.timestamp = timestamp;
    } 
    
    @Override
    public String toString() {
        return "{ value : \""+ this.value +
"\", unit : \"" + this.unit +
"\", timestamp : \"" + this.timestamp +
"\" }";
    }
    
}
