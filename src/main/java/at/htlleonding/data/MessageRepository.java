/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlleonding.data;

import static at.htlleonding.mqtt2http.StartUpHttpServer.BASE_URI;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.LinkedList;
import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 *
 * @author Rene
 */
public class MessageRepository {
    public static LinkedList<Message> messages = new LinkedList();
    private static final File indexTemplate = new File("public/indexTemplate.html");
    
    private MessageRepository() {
    }
    
    public static String generateHtml() throws IOException {
        Document templateDoc = Jsoup.parse(indexTemplate, "UTF-8", BASE_URI);
        Document doc = templateDoc;
        Elements messagesDiv = doc.select("#messages");
        messagesDiv.empty();
        return doc.outerHtml();
    }
    public static String generateDataHtmlFromMessages() throws IOException {
        Document templateDoc = Jsoup.parse(indexTemplate, "UTF-8", BASE_URI);
        Document doc = templateDoc;
        Elements messagesDiv = doc.select("#messages");
        messagesDiv.empty();  
        LinkedList<Message> reverseMessages= new LinkedList();
        if (messages.size() > 1) {
            for(int i = messages.size()-1; i>=0; i--)
            {
                reverseMessages.add(messages.get(i));
            }
            //innen/eg/speisekammer/luftfeuchtigkeit
            for(Message m : reverseMessages){
                String actTopic = m.topic;
                String[] actTopicSplit = actTopic.split("/");
                if (actTopicSplit.length > 3) {
                    String floor = actTopicSplit[1].toUpperCase();
                    String room = Character.toUpperCase(actTopic.split("/")[2].charAt(0)) + actTopic.split("/")[2].substring(1);
                    String spec = Character.toUpperCase(actTopic.split("/")[3].charAt(0)) + actTopic.split("/")[3].substring(1);
                    m.topic =  floor + ": " + room + " - " + spec;
                }
            }  
        }
        String messagesStr = "<!--?xml version=\"1.0\" encoding=\"UTF-8\"?-->"
                + "<html><head><link rel=\"stylesheet\" href=\"design/design.css\"><link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"></head>"
                + "<body class=\"body scrollable\"><table id=\"messages\">";
        for (Message message : reverseMessages) {
            messagesStr += "<tr><td><div class=\"item panel panel-default\"><div class=\"panel-body\"><h9>" + message.topic + "</h9><h2 class=\"itemh2\">" + message.value + " " + message.unit + "</h2>\n" + "<h9>" + message.timestamp + "</h9></div></div></td></tr>";
        }
        messagesStr += "</table></body></html>";
        return messagesStr;
    }

    public static void setDummyData() {
        messages.add(new Message("","5", "°C", "DD.mm.YYY hh:MM"));
        messages.add(new Message("","15", "°K", "DD.mm.YYY hh:MM"));
        messages.add(new Message("","50", "%", "DD.mm.YYY hh:MM"));
        messages.add(new Message("","50", "°C", "DD.mm.YYY hh:MM"));
        messages.add(new Message("","15", "°C", "DD.mm.YYY hh:MM"));
    }
    
}
