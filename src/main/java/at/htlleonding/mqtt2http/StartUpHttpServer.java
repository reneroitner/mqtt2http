/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlleonding.mqtt2http;

import at.htlleonding.data.MessageRepository;
import java.io.IOException;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.glassfish.grizzly.http.io.NIOWriter;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;
import org.glassfish.grizzly.http.server.StaticHttpHandler;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;

/**
 *
 * @author Rene
 */
public class StartUpHttpServer {
    public static String BASE_URI = "http://localhost:8080";
    private static PahoClient mqttClient;
    private static HttpServer httpServer;

    public static void main(String[] args) throws IOException, MqttException {
        //MessageRepository.setDummyData();
        MessageRepository.generateHtml();
        startMqttClient();
        startHttpServer();
        
        httpServer.shutdown();
        mqttClient.disconnectSubClient();
    }

    private static void startHttpServer() throws IOException {
        httpServer = GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI));
        httpServer.getServerConfiguration().addHttpHandler(new HttpHandler() {
            private String responseStr;
            
            @Override
            public void service(Request request, Response response) throws Exception {
                String reqPar = request.getParameter("q");
                if (reqPar != null && reqPar.equalsIgnoreCase("d")) {
                    responseStr = MessageRepository.generateDataHtmlFromMessages();
                } else {
                    responseStr = MessageRepository.generateHtml();
                }
                response.suspend();
                final NIOWriter out = response.getNIOWriter();
                response.setCharacterEncoding("UTF-8");
                out.write(responseStr);
                response.resume();
            }
        });
        httpServer.getServerConfiguration().addHttpHandler(new StaticHttpHandler("public"), "/"); 
        System.out.println(String.format("Server listening on: " + BASE_URI));
        System.out.println("Zum Beenden Taste drücken!");
        System.in.read();
    }

    private static void startMqttClient() throws IOException {
        try {
            mqttClient = new PahoClient();
            mqttClient.subscribeOnAllTopics();
        } catch (MqttException ex) {
            Logger.getLogger(StartUpHttpServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
