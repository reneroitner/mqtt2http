/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlleonding.mqtt2http;
import at.htlleonding.data.Message;
import at.htlleonding.data.MessageRepository;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Rene
 */
public class PahoClient implements MqttCallback {
    MqttClient subClient;
    private static final File connectionConfig = new File("config/connection.txt");
    private static final File topicConfig = new File("config/subTopic.txt");

    public PahoClient() {
    }
    
    public void subscribeOnAllTopics() throws MqttException, IOException{
        String connectionString = FileUtils.readFileToString(connectionConfig, "UTF-8");
        String topicString = FileUtils.readFileToString(topicConfig, "UTF-8");
        subClient = new MqttClient(connectionString, "Subscribing");
        subClient.connect();
        subClient.setCallback(this);
        subClient.subscribe(topicString);
    }
    
    public void disconnectSubClient() throws MqttException{
        subClient.disconnect();
        subClient.close();
    }

    @Override
    public void connectionLost(Throwable cause) {
        try {
            throw cause;
        } catch (Throwable ex) {
            Logger.getLogger(PahoClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param topic
     * @param message
     * @throws Exception
     */
    @Override
    public void messageArrived(String topic, MqttMessage message)
            throws Exception {  
        try {
            JSONObject obj = new JSONObject(message.toString());
            String value = obj.getString("value");
            String unit = obj.getString("unit");
            String timestamp = obj.getString("timestamp");
            MessageRepository.messages.add(new Message(topic,value,unit,timestamp));
            MessageRepository.generateDataHtmlFromMessages();
        }
        catch(JSONException | IOException ex){
            System.out.println("Exception in messageArrived:\n"+topic+"\n"+message.getPayload().toString());
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        // TODO Auto-generated method stub
    }
}
