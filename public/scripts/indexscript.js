
// Create a client instance
var client = new Paho.MQTT.Client("192.168.188.23", 1884, "mqttClient_" + (Math.random() + 1).toString(36).substring(7));


// set callback handlers
client.onConnectionLost = onConnectionLost;
client.onMessageArrived = onMessageArrived;

// connect the client
client.connect({onSuccess:onConnect});

// called when the client connects
function onConnect() {
  // Once a connection has been made, make a subscription and send a message.
  client.subscribe("system/config/alltopics");
  //client.subscribe("+/+/+/licht");
  //client.subscribe("innen/eg/wohnzimmer/licht/set/1");
  client.subscribe("#");
  
  var message = new Paho.MQTT.Message("{}");
  message.destinationName = "system/config/alltopicsget";
  client.send(message);
}

// called when the client loses its connection
function onConnectionLost(responseObject) {
  if (responseObject.errorCode !== 0) {
    console.log("onConnectionLost:"+responseObject.errorMessage);
  }
}




var topicLines = new Array(50);
var topicNames = new Array(50);
var topicLightExists = false;
var cntLines = 0;

// called when a message arrives
function onMessageArrived(message) {
    //console.log("message (destination):"+message.destinationName);
    //console.log("message (payload):"+message.payloadString);
    var massageTest = '[ { "topic": "innen/eg/wohnzimmer/temperatur" }, { "topic": "innen/eg/wohnzimmer/lautstaerke" }, { "topic": "innen/eg/wohnzimmer/luftfeuchtigkeit" }, { "topic": "innen/eg/wohnzimmer/licht" }, { "topic": "innen/og/badezimmer/temperatur" }, { "topic": "innen/og/badezimmer/lautstaerke" }, { "topic": "innen/og/badezimmer/luftfeuchtigkeit" }, { "topic": "innen/og/badezimmer/licht" }, { "topic": "innen/eg/kueche/temperatur" }, { "topic": "innen/eg/kueche/lautstaerke" }, { "topic": "innen/eg/kueche/luftfeuchtigkeit" }, { "topic": "innen/eg/kueche/licht" }, { "topic": "innen/eg/vorraum/temperatur" }, { "topic": "innen/eg/vorraum/lautstaerke" }, { "topic": "innen/eg/vorraum/luftfeuchtigkeit" }, { "topic": "innen/eg/vorraum/licht" }, { "topic": "innen/eg/speisekammer/temperatur" }, { "topic": "innen/eg/speisekammer/lautstaerke" }, { "topic": "innen/eg/speisekammer/luftfeuchtigkeit" }]'; //dummy data
    
    //Checken ob message ein licht ein/aus status ist, der hereinkommt
    var topicDestinationName = "";
    if(message.destinationName !== undefined){
        var temp = message.destinationName.split("/");
        //topicDestinationName = temp[3]+"/"+temp[4];// ^= licht/set
    }
    
    // GetAllTopics --
    if(message.destinationName === "system/config/alltopics"){
        var allTopics = JSON.parse(message.payloadString);//'[ { "topic": "innen/eg/wohnzimmer/temperatur" }, { "topic": "innen/eg/wohnzimmer/licht" }]'
        //var allTopics = JSON.parse(massageTest);
        for (var i = 0; i < allTopics.length; i++) {
            var topicCurrent = allTopics[i];
            var topicCurrentString = topicCurrent.topic;// innen/eg/wohnzimmer/temperatur
            var x = topicCurrentString.split("/");
            checkIfLightExists(topicCurrentString);
            if (x[3] === "licht" && !topicLightExists) {
                
                topicLines[cntLines] = topicCurrentString;
                topicNames[cntLines] = x[2];
                
                topicNames[cntLines] = topicNames[cntLines].replace('ue', 'ü');
                topicNames[cntLines] = topicNames[cntLines].replace('ae', 'ä');
                topicNames[cntLines] = topicNames[cntLines].replace('oe', 'ö');
                topicNames[cntLines] = decode_utf8(topicNames[cntLines]);
                
                cntLines++;
            }
        }    
        
        fillArrLights();
        make_tableControll();
    }
    //Licht wird ein- oder ausgeschaltet  
    else if(temp[3]==="licht" && temp[4]==="set") {
        // 'innen/eg/wohnzimmer/licht/set/1' ODER:
        // 'innen/eg/kueche/licht/set/0'
        var topic = message.destinationName;
        var x1 = topic.split("/");
        var topicForLight = x1[0]+x1[1]+x1[2];//innenegwohnzimmer
        for (var i = 0; i < topicLines.length && topicLines[i] !== undefined; i++) {
            var currentLine = topicLines[i];
            var x2 = currentLine.split("/");
            var topicForLightCurrent = x2[0]+x2[1]+x2[2];//innenegwohnzimmer
            
            var jsonobject = JSON.parse(message.payloadString);
            var value = jsonobject.value;
            if(topicForLight === topicForLightCurrent){
                if(value === "0"){//licht ist ausgeschaltet
                    document.getElementById("unchecked"+i).checked = false;
                }
                if(value === "1"){//licht ist eingeschaltet
                    document.getElementById("unchecked"+i).checked = true;
                }
            }
        }
    }  
}

function checkIfLightExists(currentLine){
    for (var i = 0; i < topicLines.length; i++) {
        if(currentLine === topicLines[i]){
            topicLightExists = true;
            return;
        }
    }
    topicLightExists = false;
}

//UTF-8 EN- und DEcode:
function encode_utf8(s) {
  return unescape(encodeURIComponent(s));
}
function decode_utf8(s) {
  return decodeURIComponent(escape(s));
}

var arrLights;
function fillArrLights(){
    var cnt = 0;
    for (var i = 0; i < topicNames.length; i++) {
        if (topicNames[i] !== undefined) {
            cnt++;
        }
    }
    
    arrLights = new Array(cnt);
    
    for (var i = 0; i < arrLights.length; i++) {
        arrLights[i] = topicNames[i];
        arrLights[i] = arrLights[i].charAt(0).toUpperCase() + arrLights[i].slice(1);
    }
}

//Dummy-Data:
//arrLights = ["Esszimmer", "Schlafzimmer Dany", "Schlafzimmer Rene", "Küche", "Terrasse", "Mo Spielzimmer"];


function oncreate(){
    //setlights();
    fillArrLights();
    make_tableControll();
}
oncreate();

function setlights(){
    /*var x = document.getElementById("selectBoxLight");
    for(i = 0; i < arrLights.length; i++){
        var option = document.createElement("option");
        option.text = arrLights[i];
        x.add(option);

    }*/
}

//Licht mithilfe der id ein/ausschalten (Zahl der ID hinten entspricht Position in 'var topicLines = new Array(50);'
function toggle_light(id) 
{
    var x = id.split("d");
    var positionIn_topicLines = parseInt(x[1]);
    topicLineCurrent = topicLines[positionIn_topicLines];
    var x1 = topicLineCurrent.split("/");
    var destinationName;
    var message;
    
    if(document.getElementById(id).checked == true) {        
        message = new Paho.MQTT.Message("{\"value\":\"1\",\"unit\":\"\",\"timestamp\":\"\"}");
        message.destinationName = x1[0] + "/" + x1[1] + "/" + x1[2] + "/" + x1[3] + "/" + "set";
        console.log("Licht AN: "+message.destinationName);
        client.send(message);
        
        
        /*message.destinationName = x1[0] + "/" + x1[1] + "/" + x1[2] + "/" + x1[3] + "/" + "set" + "/" + "1";
        console.log("Licht AN: "+message.destinationName);
        client.send(message);*/
    }
    if (document.getElementById(id).checked == false) {
        message = new Paho.MQTT.Message("{\"value\":\"0\",\"unit\":\"\",\"timestamp\":\"\"}");
        message.destinationName = x1[0] + "/" + x1[1] + "/" + x1[2] + "/" + x1[3] + "/" + "set";
        console.log("Licht AUS: "+message.destinationName);
        client.send(message);
        
        /*message.destinationName = x1[0] + "/" + x1[1] + "/" + x1[2] + "/" + x1[3] + "/" + "set" + "/" + "0";
        console.log("Licht AUS: "+message.destinationName);
        client.send(message);*/
    }
}

// <div class="styled-select"> befüllen:
function make_tableControll() {
    var result = "<table class=\"tableControll\">";
    result += "<tr>"
            + "<td>"
            + "<h1 class=\"h1tableControll\">" + "Licht" + ":</h1>"
            + "</td>"
            + "</tr>";
    
    for(i=0; i < arrLights.length; i++) {
        result += "<tr>";
        
        result += "<td class=\"tableControllTd\">"
                + "<h1 class=\"lightHeader\">" + arrLights[i] + "</h1>"
                +"</td>";
        
        //Toggle-Button: http://www.cssscript.com/material-style-toggle-switches-with-pure-css-css3/
        result += "<td class=\"tableControllTd tableControllTdRight\">"
                + "<div class=\"row demo\">"
                + "<input type=\"checkbox\" id=\"unchecked"+i+"\" class=\"cbx hidden\" onclick=\"toggle_light(id)\"/>"
                + "<label for=\"unchecked"+i+"\" class=\"lbl\"></label>"
                + "</div>"
                +"</td>";
        
        result += "</tr>";
    }
    result += "</table>";
    
    document.getElementById("tableControll").innerHTML = result;
    //return result;
}

//Button 'Reload Data':
function refreshIframe() {
    var ifr = document.getElementsByName('data')[0];
    ifr.src = ifr.src;
}

